
/**
 * WSActualizarDireccionSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.ncainvm6;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSActualizarDireccionSkeleton java skeleton for the axisService
     */
    public class WSActualizarDireccionSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSActualizarDireccionLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoDireccion
                                     * @param latitud
                                     * @param longitud
                                     * @param estadoGeorreferenciacion
                                     * @param codigoComuna
                                     * @param codigoBarrio
                                     * @param codigoManzana
                                     * @param agregado
                                     * @param remanente
         */
        

                 public co.net.une.www.ncainvm6.GisRespuestaGeneralType actualizarDireccion
                  (
                  java.lang.String codigoDireccion,java.lang.String latitud,java.lang.String longitud,java.lang.String estadoGeorreferenciacion,java.lang.String codigoComuna,java.lang.String codigoBarrio,java.lang.String codigoManzana,java.lang.String agregado,java.lang.String remanente
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoDireccion",codigoDireccion);params.put("latitud",latitud);params.put("longitud",longitud);params.put("estadoGeorreferenciacion",estadoGeorreferenciacion);params.put("codigoComuna",codigoComuna);params.put("codigoBarrio",codigoBarrio);params.put("codigoManzana",codigoManzana);params.put("agregado",agregado);params.put("remanente",remanente);
		try{
		
			return (co.net.une.www.ncainvm6.GisRespuestaGeneralType)
			this.makeStructuredRequest(serviceName, "actualizarDireccion", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    